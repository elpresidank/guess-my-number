let secretNumber = Math.trunc(Math.random() * 20) + 1;
let score = 20;
let highscore = 0;

const displayMessage = (message) => {
  document.querySelector('.message').textContent = message;
};

const changeDisplayNumber = (value) => {
  document.querySelector('.number').textContent = value;
};

const changeDisplayScore = (score) => {
  document.querySelector('.score').textContent = score;
}

const success = () => {
    displayMessage('🎉 Correct Number!');
    changeDisplayNumber(secretNumber);
    document.querySelector('body').style.backgroundColor = '#60b347';
    document.querySelector('.number').style.width = '30rem';
};

const resetGame = () => {
  secretNumber = Math.trunc(Math.random() * 20) + 1;
  score = 20;
  displayMessage('Start guessing...');
  changeDisplayScore(score);
  document.querySelector('.guess').value = '';
  changeDisplayNumber('?');
  document.querySelector('body').style.backgroundColor = '#222222';
  document.querySelector('.number').style.width = '15rem';
}

document.querySelector('.check').addEventListener
('click', function () {
  const guess = Number(document.querySelector('.guess').value);
  console.log(guess, typeof guess);

  // When there is no input.
  if (!guess) {
    displayMessage("⛔️ No Number!");

    // When player wins.
  } else if (guess === secretNumber) {
    success();

    if (score > highscore) {
      highscore = score;
      document.querySelector('.highscore').textContent = highscore;
    }

    // When guess is wrong
  } else if (guess !== secretNumber) {
    if (score > 1) {
      displayMessage(guess > secretNumber ? '📈 Too high!' : '📉 Too low!');
      score--;
      changeDisplayScore(score);
    } else {
      displayMessage('💥 You lost the game!');
      changeDisplayScore(0);
    }
  }
});

document.querySelector('.again').addEventListener('click', function () {
  resetGame();
})